# Overview
This project is the creation of a Docker image that is based on Ubuntu 20.04.2 LTS, and is tuned to be suitable as a generic base for the
more specific java based Docker images (e.g. https://bitbucket.org/leonjohan3/activemq-image). See https://bitbucket.org/leonjohan3/profile/wiki/ActiveMQ%20Docker%20and%20s2i for usage.

# Notes
1.  To install/create the image run `make`
2.  To inspect the image run `docker run -ti --rm -u default ubuntu-java-generic`
3.  Java 11 is the default version for Ubuntu 20.04.2 LTS

# To upgrade
1.  Create a new branch for the current version in case this version is required in the future
2.  Visit Ubuntu docker (maybe https://hub.docker.com/_/ubuntu/) to find out what the appropriate image tag would be (e.g. focal-20210217)
3.  See what suggestions they have to set the locale or any other settings (e.g. time zone)
4.  Copy these into the `Dockerfile` if they are different and test them
5.  Make sure the Java version is the desired version
6.  Update the repo description, this README and the version in the Dockerfile
