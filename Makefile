IMAGE_NAME = ubuntu-java-generic
DOCKER_REPO = leonjohan3atyahoodotcom/ubuntu-java-generic

.PHONY: build
build:
	docker build --progress plain -t $(IMAGE_NAME) .
	TAG=$$(docker image ls $(IMAGE_NAME) | head -2 | tail -1 | awk '{print $$3}') && docker tag $$TAG $(DOCKER_REPO)

.PHONY: deploy
deploy: build
	docker push $(DOCKER_REPO)
